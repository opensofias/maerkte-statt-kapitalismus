ISBN 978-1-57027-242-4

Gary Chartier and Charles W. Johnson
*Markets Not Capitalism: Individualist Anarchism against Bosses, Inequality, Corporate Power, and Structural Poverty*
Includes bibliographic references
1. Economics – philosophy. 2. Socialism. 3. Capitalism. 4. Anarchism. I.
Title II. Johnson, Charles W. III. Chartier, Gary

Cover image based on an image by Walter Baxter.

Released by Minor Compositions, London / New York / Port Watson
Minor Compositions is a series of interventions & provocations drawing from autonomous politics, avant-garde aesthetics, and the revolutions of everyday life.

Minor Compositions is an imprint of Autonomedia
www.minorcompositions.info | info@minorcompositions.info

Distributed by Autonomedia
PO Box 568 Williamsburgh Station
Brooklyn, NY 11211

Phone/fax: 718-963-0568
www.autonomedia.org
info@autonomedia.org
