# Minor Compositions Open Access Statement – Please Read

This book is open access. This work is not simply an electronic book; it is the open access version of a work that exists in a number of forms, the traditional printed form being one of them.

All Minor Compositions publications are placed for free, in their entirety, on the web. This is because the free and autonomous sharing of knowledges and experiences is important, especially at a time when the restructuring and increased centralization of book distribution makes it difficult (and expensive) to distribute radical texts effectively.

The free posting of these texts does not mean that the necessary energy and labor to produce them is no longer there. One can think of buying physical copies not as the purchase of commodities, but as a form of support or solidarity for an approach to knowledge production and engaged research (particularly when purchasing directly from the publisher).

The open access nature of this publication means that you can:
* read and store this document free of charge
* distribute it for personal use free of charge
* print sections of the work for personal use
* read or perform parts of the work in a context where no financial transactions take
place

However, it is against the purposes of Minor Compositions open access approach to:
* gain financially from the work
* sell the work or seek monies in relation to the distribution of the work
* use the work in any commercial activity of any kind
* profit a third party indirectly via use or distribution of the work
* distribute in or through a commercial body (with the exception of academic usage within educational institutions)

The intent of Minor Compositions as a project is that any surpluses generated from the use of collectively produced literature are intended to return to further the development and production of further publications and writing: that which comes from the commons will be used to keep cultivating those commons. Omnia sunt communia!
Support Minor Compositions / Purchasing Books
The PDF you are reading is an electronic version of a physical book that can be
purchased through booksellers (including online stores), through the normal book
supply channels, or Minor Compositions directly. Please support this open access
publication by requesting that your university or local library purchase a physical printed
copy of this book, or by purchasing a copy yourself.
If you have any questions please contact the publisher: minorcompositions@gmail.com.
