# Märkte statt Kapitalismus

Projekt für eine deutsche Übersetzung von Markets not Capitalism

Englische PDF: http://radgeek.com/gt/2011/10/Markets-Not-Capitalism-2011-Chartier-and-Johnson.pdf

Englisches Hörbuch: https://archive.org/details/MarketsNotCapitalismAudiobook

## Roadmap

* Quellentexte als Markdown importieren. Üblicherweise wird es eine Web-Fassung der Essays geben, aus denen der Text leichter zu extrahieren ist als aus der PDF.
* Sämtliche Texte auf deutsch übersetzen. Maschinelle Dienste wie DeepL können dabei sehr behilflich sein.
